Fedora 25 - Software Collections Package Repository
===================================================

Repository Setup
----------------

Install with dnf repository.

``` shell
% sudo tee -a /etc/yum.repos.d/kjdev.repo <<EOM
[kjdev-scl]
name=Fedora \$releasever - kjdev - SCL
baseurl=https://gitlab.com/kjdev/fc\$releasever.scl/raw/master/rpm/
enabled=0
gpgcheck=0

[kjdev-scl-source]
name=Fedora \$releasever - kjdev - SCL - Source
baseurl=https://gitlab.com/kjdev/fc\$releasever.scl/raw/master/source/
enabled=0
gpgcheck=0
EOM
```

RPM Install
-----------

``` shell
% dnf --enablerepo kjdev-scl install <PACKAGE>
```

Use SCL
-------

``` shell
% scl enable php71 'php -v'
PHP 7.0.13 (cli)
```

> Installed: `dnf --enablerepo kjdev-scl install php71-php-cli`


### Apache and PHP

Install `php`.

``` shell
% dnf --enablerepo kjdev-scl install php71-php
```

Uncomment the `/etc/httpd/conf.d/php71.conf`.

``` conf
# /etc/httpd/conf.d/php71.conf
IncludeOptional /opt/kj/php71/root/etc/httpd/conf.modules.d/10-php.conf
IncludeOptional /opt/kj/php71/root/etc/httpd/conf.d/php.conf
```

Run apache.

``` shell
% /usr/sbin/httpd -DFOREGROUND
```

### Nginx and PHP-FPM

Install `php-fpm`.

``` shell
% dnf --enablerepo kjdev-scl install php71-php-fpm
```

Run PHP-FPM.

``` shell
% scl enable php71 'php-fpm --nodaemonize'
% : OR /opt/kj/php71/root/usr/sbin/php-fpm --nodaemonize
```

Uncomment the `/etc/nginx/default.d/php71.conf`.

``` conf
# /etc/nginx/default.d/php71.conf
include /opt/kj/php71/root/etc/nginx/default.d/php.conf;
```

Run nginx.

``` shell
% /usr/sbin/nginx -g 'daemon off;'
```
